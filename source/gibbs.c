#include "gibbs.h"
              			
#define kb 1.3806505E-23                // Boltzman constant
#define H 1.6E-19                    // convert the eV to J
#define h 6.6260755E-34             // reduced Planck's constant
#define w 1.0E12                    // THz to Hz
#define N_a 6.02E23                  //Avgavdlo constant
#define PI 3.14159265083
#define e_c 1.60217646E-19         // Unit C, elementary charge

int Gibbs(){
	int i,j,l;
	int rows;  // number of rows in Phonon dos file
	int rowss; // number of rows in electron DOS file
	int divisor; // to get one single crystal structure
    float E_dft; //DFT total energy at 0K
 	char filename1[256],filename2[256]; // input file name
	char filename33[256]; //output file name
	char filename[256];
	double T, T_min, T_max, T_step; 
	float dummy_x,dummy_y,dummy_z;
    double *omega,*pdos;
	double *energy,*tdos;
	double E_zpe; // zero point energy
	double H_ph; // phonon enthalpy
	double U_ph; // phonon internal energy
	double S_ph; // phonon entropy
	double F_ph; // Hemoltz free energy
        double S_conf;
	double H_el, U_el, S_el, F_el; // electron parts
	double e_dft;
        double ma;
        double u;    
// Auxiliary function
	double x,y,z;
	float f(double, double, double);  // Fermi-Dirac distribution function
	FILE *input1;
    FILE *input2; 
	FILE *output3;
	FILE *output;
// temperautre settings
    T_min = 2.0 ; 
	T_max = 2000.0 ;
	T_step = 2.0 ;
	char line[2048]; 
// Start the calculation
	FILE *input;
	char  *words;
	int  size;
	input = fopen ("in/INPUT", "r");
	for (i = 0;i < 8; i ++) fgets (line, 2048, input);
        sscanf (line, "%f",&E_dft);
        fgets (line, 2048, input);
	sscanf (line, "%d",&divisor);
        fgets (line, 2048, input);
	sscanf (line, "%lg",&ma);
        fgets (line, 2048, input);
	sscanf (line, "%lg",&u);
	fclose (input);
// read the input files
    int Ss;
//    while((Ss=getchar())!=EOF&&Ss!='\n');
//    scanf("%s", filename1);
    sprintf (filename1, "in/total_dos.dat");
    input1=fopen(filename1,"r");
     if(input1==NULL){
	                  perror("Error while opening the file.\n");
                      getchar();
                      exit(EXIT_FAILURE);
                     }
    int A=0;
    input1=fopen(filename1,"r");
    fgets (line, 2048, input1);
    while(!feof(input1)){
                        fscanf(input1,"%f %f",&dummy_x, &dummy_y);
                        A++;
                       }
    rows=A-1;
    fclose(input1);
// allocate the dynamic memory to the arraies
    omega=(double*)malloc(rows*sizeof(double));
    pdos=(double*)malloc(rows*sizeof(double));
    input1=fopen(filename1,"r");
    fgets (line, 2048, input1);
    for(i=0;i<rows;i++){
                        fscanf(input1,"%lf %lf",&(omega[i]),&(pdos[i]));
					   }
	fclose(input1);
	printf("Phonon DOS file is uploaded\n");
    int S1;
//    while((S1=getchar())!=EOF&&S1!='\n');
//    scanf("%s", filename2);
    sprintf (filename2, "in/TDOS.dat");
    input2=fopen(filename2,"r");
    if(input2==NULL){
                     perror("Error while opening the file.\n");
                     getchar();
                    exit(EXIT_FAILURE);
                    }
    int B=0;
    fgets (line, 2048, input2);
    while(!feof(input2)){
                         fscanf(input2,"%f %f",&dummy_x, &dummy_y);
                         B++;
                        }
    rowss=B-1;
    fclose(input2);
// allocate the dynamic memory to the arraies
    energy=(double*)malloc(rowss*sizeof(double));
    tdos=(double*)malloc(rowss*sizeof(double));
    input2=fopen(filename2,"r");
    fgets (line, 2048, input2);
    for(i=0;i<rowss;i++){
                         fscanf(input2,"%lf %lf",&(energy[i]),&(tdos[i]));
						}
	fclose(input2);
	printf("Electron DOS file is uploaded\n");
// For computing electronic entropy, we only need TDOS near the Fermi surface 
    double R=(1.6021764/1.3806505)*pow(10.0,4);
	sprintf(filename33,"out/1/G_T.dat");
	output3=fopen(filename33,"w");
	T=0;
	for(i=0;i<rows;i++){
             	        // negative frequency is evil.
						if(omega[i]>0.0){
										 E_zpe += (1/2.0)*w*h*(1.0/H)*(pdos[i]*omega[i]*(omega[i]-omega[i-1]));
										}
					   }
	e_dft=(E_dft+E_zpe)/divisor;
	fprintf(output3,"%f\t%f\n",T,e_dft);
	for(T=T_min;T<=T_max;T=T+T_step){
									 E_zpe=0.000000; U_ph=0.000000;
             	                     F_ph = 0.000000; S_ph=0.000000;
									 for(i=1;i<rows;i++){
             	                                         // negative frequency is evil.
														 if(omega[i]>0.0){
             	                                                           double g_1=1.0/(exp(h*w*omega[i]/(kb*T))-1.0);
                                                                           double p_1=1.0-exp(-omega[i]*w*h/(kb*T));
																		   E_zpe += (1/2.0)*w*h*(1.0/H)*(pdos[i]*omega[i]*(omega[i]-omega[i-1]));
                                                                           U_ph += (1/H)*(h*w*omega[i]*pdos[i])*(g_1)*(omega[i]-omega[i-1]);
                                                                           S_ph += (1/H)*((-kb*pdos[i]*log(p_1)*(omega[i]-omega[i-1]))+(h*w*omega[i]*pdos[i]/T)*g_1*(omega[i]-omega[i-1]));
                                                                           S_conf=(-1/H)*ma*kb*((u+0.0000000001)*log((u+0.0000000001))+(1-u)*log(1-u));
																		 }
														}
									double Ucp1=0.0000;
                                    double Ucp2=0.0000;
                                    for(j=0;j<rowss;j++){                                                                          
                                                          Ucp1 += tdos[j]*fabs(energy[j]-energy[j-1])*energy[j]*f(energy[j],R,T);
                                                          if(energy[j]<=0.0){
                                                                              Ucp2 += tdos[j]*fabs(energy[j]-energy[j-1])*energy[j];
                                                                            }                                                                   
                                                        }
                                    U_el=Ucp1-Ucp2;
                                    // Pick up the DOS at Fermi surface
                                    int rown;
                                    double *energy1,*tdos1;
									sprintf(filename,"out/1/Fermi_dos");
                                    output=fopen(filename,"w");
									rown=0;
									for(i=0;i<rowss;i++){
                                                         if(energy[i]>-10.0*(1.38/1.6)*pow(10.0,-4.0)*T&&energy[i]<10.0*(1.38/1.6)*pow(10.0,-4.0)*T){
                                                           	                    	                                                                  rown++;
                                                           	                    	                                                          fprintf(output,"%lf\t%lf\n",energy[i],tdos[i]);
                                                           	                                                                                    }
                                                        }
									fclose(output);
									energy1=(double*)malloc(rown*sizeof(double));
									tdos1=(double*)malloc(rown*sizeof(double));
									output=fopen(filename,"r");
									for(i=0;i<rown;i++){
														fscanf(output,"%lf %lf",&(energy1[i]),&(tdos1[i]));
													   } 
									fclose(output);  
	                                // electronic entropy
                                    S_el=0.000000;
                                    for(l=0;l<rown;l++){
                                                        // if(energy[l]>-10.0*(1.38/1.6)*pow(10.0,-4.0)*T&&energy[l]<10.0*(1.38/1.6)*pow(10.0,-4.0)*T){
                                                        double F_1=1.0-f(energy1[l],R,T);
                                                        double F_2=f(energy1[l],R,T);
                                                        S_el+=(-(1.3806505/1.6)*pow(10.0,-4)*((F_1)*log(F_1)+F_2*log(F_2))*tdos1[l]*fabs(energy1[l]-energy1[l-1]));
                                                        }
									free(energy1); free(tdos1);
									double U_t = E_dft+E_zpe+U_ph+U_el;
									double S_t = S_ph+S_el+S_conf;
									double G_t = (U_t-T*S_t)/divisor;
								    fprintf(output3,"%f\t%f\n",T,G_t);
                                    }
    free(omega); free(pdos);
    free(energy); free(tdos);
	fclose(output3); 
    system ("rm out/1/Fermi_dos");
    printf("All calculations are done!\n");
    return 0;
}
// Auxilary function
float f(double x, double y, double z)
{
      return 1.0/(exp((x*y)/z)+1.0);
      }
