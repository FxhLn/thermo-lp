#include "main.h"

Phase *pha, cp;
int Nphase, gp_type_id, gp_id = -1, std_id = -1, Ncp, choice, path_flag = 0;
double temperature, Egoal = 0., deltaE, Ethreshold, *comb_coe, Esim_sub, Ereactant[2000], Treactant[2000], Eproduct[2000], Tproduct[2000];
char gp_type[10], gp_name[128], reactant[1024], product[1024];
FILE *path_file;

int main (int argc, char **argv)
{
	Start ();
	ReadINPUT ();
	system ("mkdir out");
	switch (choice) {
		case 1:
			system ("rm -rf out/1 && mkdir out/1");
			Gibbs();
			break;
		case 2:
			system ("rm -rf out/2 && mkdir out/2");
			ReadINPUT ();
			ReadName ();
			CompetingPhases ();
			Project ();
			break;
		case 3:
			system ("rm -rf out/3 && mkdir out/3");
			ReadINPUT ();
			ReadName ();
			CompetingPhases ();
			SearchImpurityPhase ();
			break;
		default:
			break;
	}
	return 0;
}

void init (int argc, char **argv)
{
	int i;

	printf ("input the number of elements:\n");
	cp.N = 4;
//	scanf ("%d", &cp.N);
	printf ("input the elements:\n");
	sprintf (cp.element[0], "Cr");
	sprintf (cp.element[1], "Ti");
	sprintf (cp.element[2], "Al");
	sprintf (cp.element[3], "C");
//	for (i = 0; i < cp.N; i ++) scanf ("%s", cp.element[i]);
	if (choice == 2) printf ("input the standard stoichiometric ratio:\n");
	else if (choice == 3) printf ("input the raw material ratio:\n");
	cp.coe[0] = 2;
	cp.coe[1] = 1;
	cp.coe[2] = 1;
	cp.coe[3] = 2;
//	for (i = 0; i < cp.N; i ++) scanf ("%lg", &cp.coe[i]);
	printf ("input the temperature:\n");
	temperature = 1700;
//	scanf ("%lg", &temperature);
	printf ("the target phase is o-MAX(1), SQS(2), i-MAX(3) or conventional compound(4)\n");
	gp_type_id = 1;
//	scanf ("%d", &gp_type_id);
	switch (gp_type_id) {
		case 1:
			strcpy (gp_type, "o-MAX");
			break;
		case 2:
			strcpy (gp_type, "SQS");
			break;
		case 3:
			strcpy (gp_type, "i-MAX");
			break;
		case 4:
			strcpy (gp_type, "\0");
			break;
		default:
			break;
	}
//	printf ("input energy threshold:(eV)\n");
	Ethreshold = 0.015;
//	scanf ("%lg", &Ethreshold);
}

void ReadName ()
{
	FILE *input;
	char line[1024], string[10];
	int i, j, k;

	input = fopen ("data_base/name.dat", "r");
	Nphase = 0;
	while (1) {
		if (fgets (line, 1024, input) == 0) break;
		Nphase ++;
	}
	printf ("the number of phases is %d\n", Nphase);
	AllocMem (pha, Nphase, Phase);
	rewind (input);
	i = 0;
	while (1) {
//		printf ("%d\n", i);
		if (fgets (line, 1024, input) == 0) break;
		sscanf (line, "%s", pha[i].name);
		strcpy (pha[i].type, "\0");
		j = 0;
		while (1) {
			if ((line[0] == 10) || (line[0] == 40)) break;

			if ((line[0] >= 65 && line[0] <= 90)) {
				//identify the element
				if ((line[1] >= 65 && line[1] <= 90) || (line[1] >= 48 && line[1] <= 57) || (line[1] == 40) || (line[1] == 10)) { //single letter element
					sprintf (pha[i].element[j], "%c", line[0]);
					sprintf (line, "%s", line+1);
				} else if (line[1] >= 97 && line[1] <= 122) { //double letter element
					sprintf (pha[i].element[j], "%c%c", line[0], line[1]);
					sprintf (line, "%s", line+2);
				}
				//identify coefficient
				if (line[0] >= 48 && line[0] <= 57) sscanf (line, "%lg", &(pha[i].coe[j]));
				else pha[i].coe[j] = 1.;
				while (1) {
					if ((line[0] == 10) || (line[0] >= 65 && line[0] <= 90) || (line[0] == 40)) break;
					else sprintf (line, "%s", line+1);
				}
			}

			//identify type
			if (line[0] == 40) {
				sprintf (line, "%s", line+1);
				strcpy (pha[i].type, "\0");
				while (1) {
					if (line[0] == 41) {
						sprintf (line, "%s", line+1);
						break;
					}
					sprintf (string, "%c", line[0]);
					strcat (pha[i].type, string);
					sprintf (line, "%s", line+1);
				}
			}

			j ++;
		}
		pha[i].N = j;
//		printf ("%s \n%d ", pha[i].name, pha[i].N);
		for (j = 0; j < pha[i].N; j ++) {
//			printf("%s %.3f ", pha[i].element[j], pha[i].coe[j]);
		}
//		printf ("%s", pha[i].type);
//		printf ("\n\n");

		i ++;
	}
	fclose (input);
}

void CompetingPhases ()
{
	FILE *input;
	int i, j, k;
	double T, E, dT, mark;
	char filename[128], filename1[128], line[1024];

	Ncp =  0;
	for (i = 0; i < Nphase; i ++) {
		pha[i].mark_cp = 1;
		for (j = 0; j < pha[i].N; j ++) {
			mark = 0;
			for (k = 0; k < cp.N; k ++) {
				if (strcmp (pha[i].element[j], cp.element[k]) == 0) mark = 1;
			}
			if (mark == 0) {
				pha[i].mark_cp = 0;
			}
		}
		if (pha[i].mark_cp == 1) Ncp  ++;
	}
	printf ("\n\n/******************competing phases********************/\n");
	printf ("\nthe number of competing phases is %d\n", Ncp);

	//read energy and experiment mark
	for (i = 0; i < Nphase; i ++) {
		if (pha[i].mark_cp == 1) {
			switch (pha[i].N) {
				case 1: 
					sprintf (filename, "data_base/single_phase/%s/E.dat", pha[i].name);
					if (access(filename, R_OK) == -1) {
						printf ("%s is not accessible\n", filename);
						exit (1);
					}
					sprintf (filename1, "data_base/single_phase/%s/1.txt", pha[i].name);
					if (access(filename1, R_OK) == 0) pha[i].mark_exp = 1;
					else pha[i].mark_exp = 0;
					sprintf (filename1, "data_base/single_phase/%s/2.txt", pha[i].name);
					if (access(filename1, R_OK) == 0) pha[i].mark_exp_easy = 2;
					else pha[i].mark_exp_easy = 0;
					break;
				case 2: 
					sprintf (filename, "data_base/binary_phase/%s/E.dat", pha[i].name);
					if (access(filename, R_OK) == -1) {
						printf ("%s is not accessible\n", filename);
						exit (1);
					}
					sprintf (filename1, "data_base/binary_phase/%s/1.txt", pha[i].name);
					if (access(filename1, R_OK) == 0) pha[i].mark_exp = 1;
					else pha[i].mark_exp = 0;
					sprintf (filename1, "data_base/binary_phase/%s/2.txt", pha[i].name);
					if (access(filename1, R_OK) == 0) pha[i].mark_exp_easy = 2;
					else pha[i].mark_exp_easy = 0;
					break;
				case 3: 
					sprintf (filename, "data_base/ternary_phase/%s/E.dat", pha[i].name);
					if (access(filename, R_OK) == -1) {
						printf ("%s is is not accessible\n", filename);
						exit (1);
					}
					sprintf (filename1, "data_base/ternary_phase/%s/1.txt", pha[i].name);
					if (access(filename1, R_OK) == 0) pha[i].mark_exp = 1;
					else pha[i].mark_exp = 0;
					sprintf (filename1, "data_base/ternary_phase/%s/2.txt", pha[i].name);
					if (access(filename1, R_OK) == 0) pha[i].mark_exp_easy = 2;
					else pha[i].mark_exp_easy = 0;
					break;
				case 4: 
					sprintf (filename, "data_base/quaternary_phase/%s/E.dat", pha[i].name);
					if (access(filename, R_OK) == -1) {
						printf ("%s is not accessible\n", filename);
						exit (1);
					}
					sprintf (filename1, "data_base/quaternary_phase/%s/1.txt", pha[i].name);
					if (access(filename1, R_OK) == 0) pha[i].mark_exp = 1;
					else pha[i].mark_exp = 0;
					sprintf (filename1, "data_base/quaternary_phase/%s/2.txt", pha[i].name);
					if (access(filename1, R_OK) == 0) pha[i].mark_exp_easy = 2;
					else pha[i].mark_exp_easy = 0;
					break;
				default:
					break;
			}
			input = fopen (filename, "r");
			dT = 1.e100;
			j = 0;
			while (1) {
				if (fgets (line, 1024, input) == 0) break;
				sscanf (line, "%lg %lg", &T, &E);
				if (dT > fabs(T - temperature)) {
					dT = fabs(T - temperature);
					pha[i].energy = E;
				}
				pha[i].T[j] = T;
				pha[i].E[j] = E;
				j ++;
			}
			pha[i].NE = j;
			printf ("%s %f\n", pha[i].name, pha[i].energy);
			fclose (input);
		}
	}
}

void Project ()
{
	int i, j, k, l, m, mark, mark1, mark2, loop, Ncons, flag, Eflag = 0;
	FILE *output;
	char filename[128], filename1[128], line[1024];
	double T, E, dT, E1, Emin, Econstraint, **coe_cons, na, dE, Eopt;

	//linear programming
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark_cp == 1) pha[i].mark = 1;
		else pha[i].mark = 0;
	}
	AllocMem2 (coe_cons, 1, Ncp, double);
	for (i = 0; i < Ncp; i ++) coe_cons[0][i] = -1.;
	Emin = LinearProgramming (-1.e10, coe_cons, 1);
	free (coe_cons[0]);
	free (coe_cons);
	//calculate reactant energy in each temperature between 0 ~ 2000K
	for (j = 0; j <= 1000; j ++) {
		E = 0;
		for (i = 0; i < Nphase; i ++)  {
			if (pha[i].mark == 1) {
				if (pha[i].comb_coe > 1.e-10) {
					E += pha[i].comb_coe * pha[i].E[j];
				}
			}
		}
		Tproduct[j] = j * 2.;
		Eproduct[j] = E;
	}
	flag = 0;
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark == 1) {
			if (pha[i].comb_coe > 1.e-10) {
				if (flag == 0) {
					sprintf (line, "%.3f %s ", pha[i].comb_coe, pha[i].name);
					strcat (product, line);
					flag = 1;
				} else {
					sprintf (line, "+ %.3f %s ", pha[i].comb_coe, pha[i].name);
					strcat (product, line);
				}
			}
		}
	}
	puts (product);

	for (i = 0; i < Nphase; i ++) {
		if (pha[i].mark_cp == 1 && pha[i].N == cp.N) {
			mark1 = 1;
			for (j = 0; j < pha[i].N; j ++) {
				mark = 0;
				for (k = 0; k < cp.N; k ++)
					if (strcmp (pha[i].element[j], cp.element[k]) == 0 && pha[i].coe[j] == cp.coe[k]) mark = 1;
				if (mark == 0) mark1 = 0;
			}
			if (mark1 == 1 && strcmp (pha[i].type, gp_type) == 0) {
				printf ("target phase: %s %f\n", pha[i].name, pha[i].energy);
				Egoal = pha[i].energy;
				strcpy(gp_name, pha[i].name);
			}
		}
	}
	if (fabs(Egoal) < 1.e-100) {
		printf ("\nThis target phase is not included in the database\n");
		exit (1);
	}

	na = 0.;
	for (i = 0; i < cp.N; i ++) {
		na += cp.coe[i];
	}
	deltaE = Egoal - Emin;
	output = fopen ("out/2/thermodynamic_stablity", "w");
	if (deltaE > Ethreshold * na) {
		printf ("The target phase %s is unstable at %.3f K\n", gp_name, temperature);
		fprintf (output, "The target phase %s is unstable at %.3f K\n", gp_name, temperature);
		return;
	} else if (fabs(deltaE) < 1.e-10){
		printf ("The target phase %s is stable at %.3f K\n", gp_name, temperature);
		fprintf (output, "The target phase %s is stable at %.3f K\n", gp_name, temperature);
	} else {
		printf ("The target phase %s is meta-stable at %.3f K\n\n",  gp_name, temperature);
		fprintf (output, "The target phase %s is meta-stable at %.3f K\n\n",  gp_name, temperature);
	}
	fclose (output);

	printf ("\n\n/******************experimentlly easily available phases********************/\n");
	for (i = 0; i < Nphase; i ++) {
		if (pha[i].mark_cp == 1 && pha[i].mark_exp_easy == 2) printf ("%s %.3f\n", pha[i].name, pha[i].energy);
	}

	//linear programming
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark_cp == 1 && pha[i].mark_exp_easy == 2) pha[i].mark = 1;
		else pha[i].mark = 0;
	}
	AllocMem2 (coe_cons, 1, Ncp, double);
	for (i = 0; i < Ncp; i ++) coe_cons[0][i] = -1.;
	printf ("\n===optimized synthesis path:\n");
	Eopt = LinearProgramming (-1.e10, coe_cons, 1);
	flag = 0;
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark == 1) {
			if (fabs(pha[i].comb_coe - 1.) < 1.e-10) {
				flag ++;
			}
		}
	}
	if (flag == 1) {
		printf ("It can be purchased directly\n");
		exit (0);
	}
	free (coe_cons[0]);
	free (coe_cons);
	printf ("\n\n");

	//calculate reactant energy in each temperature between 0 ~ 2000K
	for (j = 0; j <= 1000; j ++) {
		E = 0;
		for (i = 0; i < Nphase; i ++)  {
			if (pha[i].mark == 1) {
				if (pha[i].comb_coe > 1.e-10) {
					E += pha[i].comb_coe * pha[i].E[j];
				}
			}
		}
		Treactant[j] = j * 2.;
		Ereactant[j] = E;
	}
	flag = 0;
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark == 1) {
			if (pha[i].comb_coe > 1.e-10) {
				if (flag == 0) {
					sprintf (line, "%.3f %s ", pha[i].comb_coe, pha[i].name);
					strcat (reactant, line);
					flag = 1;
				} else {
					sprintf (line, "+ %.3f %s ", pha[i].comb_coe, pha[i].name);
					strcat (reactant, line);
				}
			}
		}
	}

	//output gbs free energy
	na = 0.;
	for (i = 0; i < cp.N; i ++) {
		na += cp.coe[i];
	}
	sprintf (filename, "out/2/target reaction.dat");
	output = fopen (filename, "w");
	fprintf (output, "%s ->\n", reactant);
	fprintf (output, "%s\n", product);
	fprintf (output, "T deltaG\n");
	for (i = 0; i <= 1000; i ++) {
		fprintf (output, "%.0f %e\n", i * 2., (Eproduct[i] - Ereactant[i]) / na);
	}
	fclose (output);

/*******************************************************************************************************/
/*****************************Thermodynamics of competitive reactions***********************************/
/*******************************************************************************************************/
	printf ("\n\n/******************Thermodynamics of competitive reactions******************/\n");
        Esim_sub = 0.;
        E1 = Emin;
	for (i = 0; i < Nphase; i ++) {
		for (j = 0; j < cp.N; j ++) {
			if (strcmp (pha[i].name, cp.element[j]) == 0) {
				Esim_sub += pha[i].energy * cp.coe[j];
			}
		}
	}
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark_cp == 1) pha[i].mark = 1;
		else pha[i].mark = 0;
	}
	AllocMem2 (coe_cons, 1, Ncp, double);
	for (i = 0; i < Ncp; i ++) coe_cons[0][i] = -1.;
	loop = 0;
	Ncons = 1;
	Econstraint = Emin;
	while (1) {
		//linear programming
		if (Eopt > E1){Emin = LinearProgramming (Econstraint, coe_cons, Ncons);
		if (Eflag == 0) {
			dE = (Eopt - Econstraint) / 10.;
			Eflag = 1;
		}
		Econstraint += dE;
		if (Econstraint >= Eopt + 0.015 * na) break;}
                if (Eopt <= E1){Emin = LinearProgramming (Econstraint, coe_cons, Ncons);
		if (Eflag == 0) {
			dE = (Egoal - Econstraint) / 10.;
			Eflag = 1;
		}
		Econstraint += dE;
		if (Econstraint >= Egoal + 0.015 * na) break;}

		//calculate gbs free energy
		na = 0.;
		for (i = 0; i < cp.N; i ++) {
			na += cp.coe[i];
		}
		sprintf (filename, "out/2/competing reaction_%d.dat", loop + 1);
		output = fopen (filename, "w");
		fprintf (output, "%s ->\n", reactant);
		flag = 0;
		for (i = 0; i < Nphase; i ++)  {
			if (pha[i].mark == 1) {
				if (pha[i].comb_coe > 1.e-10) {
					if (flag == 0) {
						fprintf (output, "%.3f %s ", pha[i].comb_coe, pha[i].name);
						flag = 1;
					} else fprintf (output, "+ %.3f %s ", pha[i].comb_coe, pha[i].name);
				}
			}
		}
		fprintf (output, "\nT deltaG\n");
		for (j = 0; j <= 1000; j ++) {
			E = 0;
			for (i = 0; i < Nphase; i ++)  {
				if (pha[i].mark == 1) {
					if (pha[i].comb_coe > 1.e-10) {
						E += pha[i].comb_coe * pha[i].E[j];
					}
				}
			}
			fprintf (output, "%.0f %e\n", j * 2., (E - Ereactant[j]) / na);
		}
		fclose (output);

		//add constraints
		Ncons ++;
		ReAllocMem2 (coe_cons, Ncons, Ncp, double);
		j = 0;
		for (i = 0; i < Nphase; i ++)  {
			if (pha[i].mark == 1) {
				if (pha[i].comb_coe > 1.e-100) coe_cons[Ncons-1][j] = 0;
				else coe_cons[Ncons-1][j] = -1;
				j ++;
			}
		}
		if (Ncons > 10) {
			for (i = 0; i < Ncons - 1; i ++) {
				for (j = 0; j < Ncp; j ++) {
					coe_cons[i][j] = coe_cons[i+1][j];
				}
			}
			Ncons --;
		}

	

		output = fopen ("out/2/compounds on every energy ladder.dat", "a+");
		fprintf (output, "E = %.2f: ", Emin);
		flag = 0;
		for (i = 0; i < Nphase; i ++)  {
			if (pha[i].mark == 1) {
				if (pha[i].comb_coe > 1.e-10) {
					if (flag == 0) {
						fprintf (output, "%.3f %s ", pha[i].comb_coe, pha[i].name);
						flag = 1;
					} else fprintf (output, "+ %.3f %s ", pha[i].comb_coe, pha[i].name);
				}
			}
		}
		fprintf (output, "\n");
		fclose (output);

		loop ++;
	}
	free (coe_cons[0]);
	free (coe_cons);
}

/*******************************************************************************************************/
/***********************************searching for impurity phase****************************************/
/*******************************************************************************************************/
void SearchImpurityPhase ()
{
	int mark1, mark2, i, j, k;
	double **coe_cons;
	double *coe_std;
	FILE *input, *output;
	char line[1024], *words;

	printf ("\n\n/*************************searching for impurity phase*****************************/\n");
	AllocMem (coe_std, cp.N, double);
	printf ("input the standard stoichiometric ratio:\n");
	input = fopen ("in/INPUT", "r");
	for (i = 0;i < 7; i ++) fgets (line, 1024, input);
	words = strtok(line,"' \t\n\r\f");
	for (i = 0; i < cp.N; i ++) {
		coe_std[i] = atof (words);
		words = strtok(NULL,"' \t\n\r\f");
		printf ("%f\n", coe_std[i]);
	}
	fclose (input);
//	for (i = 0; i < cp.N; i ++) scanf ("%lg", &coe_std[i]);
	printf ("experimentally-known competing phases:\n");
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark_cp == 1 && pha[i].mark_exp == 1) printf ("%s %f\n", pha[i].name, pha[i].energy);
	}
	//find goal phase id
	gp_id = -1;
	for (i = 0; i < Nphase; i ++) {
		if (pha[i].mark_cp == 1) {
			mark1 = 1;
			if (cp.N != pha[i].N) mark1 = 0;
			if (strcmp(gp_type, pha[i].type) != 0) mark1 = 0;
			for (j = 0; j < cp.N; j ++) {
				mark2 = 0;
				for (k = 0; k < pha[i].N; k ++) {
					if (strcmp(cp.element[j], pha[i].element[k]) == 0 && cp.coe[j] == pha[i].coe[k]) mark2 = 1;
				}
				if (mark2 == 0) mark1 = 0;
			}
			if (mark1 == 1) {
				printf ("\ntarget phase:\n%s\n", pha[i].name);
				gp_id = i;
			}
		}
	}
	//find std phase id
	std_id = -1;
	for (i = 0; i < Nphase; i ++) {
		if (pha[i].mark_cp == 1) {
			mark1 = 1;
			if (cp.N != pha[i].N) mark1 = 0;
			if (strcmp(gp_type, pha[i].type) != 0) mark1 = 0;
			for (j = 0; j < cp.N; j ++) {
				mark2 = 0;
				for (k = 0; k < pha[i].N; k ++) {
					if (strcmp(cp.element[j], pha[i].element[k]) == 0 && coe_std[j] == pha[i].coe[k]) mark2 = 1;
				}
				if (mark2 == 0) mark1 = 0;
			}
			if (mark1 == 1) {
				printf ("\nstandard phase:\n%s\n", pha[i].name);
				std_id = i;
			}
		}
	}
	//linear programming
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark_cp == 1 && pha[i].mark_exp == 1) pha[i].mark = 1;
		else pha[i].mark = 0;
	}
	pha[std_id].mark = 1; //if std phase is not included above, include it
	if (gp_id != -1) pha[gp_id].mark = 0;
	AllocMem2 (coe_cons, 1, Ncp, double);
	for (i = 0; i < Ncp; i ++) coe_cons[0][i] = -1.;
	printf ("\ncompeting phases in research:\n");
	for (i = 0; i < Nphase; i ++) {
		if (pha[i].mark == 1) puts (pha[i].name);
	}
	LinearProgramming (-1.e10, coe_cons, 1);
	printf ("\nimpurity phases:\n");
	output = fopen ("out/3/predited_impurity_phases", "a+");
	for (i = 0; i < Nphase; i ++) {
		if (pha[i].mark_imp == 1) {
			printf ("%s    ", pha[i].name);
			fprintf (output, "%s    ", pha[i].name);
		}
	}
	fclose (output);
	printf ("\n");
	
	free (coe_cons[0]);
	free (coe_cons);
	free (coe_std);
}

double LinearProgramming (double Econstraint, double **coe_cons, int Ncons)
{
	int i, j, k, l, mark, Ncp;
	FILE *input, *output;
	char line[1024];
	double Emin;
	
	output = fopen ("out/linprog.py", "w");
	fprintf (output, "from scipy.optimize import linprog\nc=[");
	Ncp = 0;
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark == 1) Ncp ++;
	}
	j = 0;
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark == 1) {
			fprintf (output, "%f", pha[i].energy);
			if (j < Ncp-1) fprintf (output, ",");
			j ++;
		}
	}
	fprintf (output, "]\nA_ub=[[");
	j = 0;
	for (i = 0; i < Nphase; i ++)  {
		if (pha[i].mark == 1) {
			fprintf (output, "%f", -pha[i].energy);
			if (j < Ncp-1) fprintf (output, ",");
			j ++;
		}
	}
	for (i = 0; i < Ncons; i ++) {
		fprintf (output, "],[");
		for (j = 0;j < Ncp - 1; j ++) {
			fprintf (output, "%.0f,", coe_cons[i][j]);
		}
		fprintf (output, "%.0f", coe_cons[i][j-1]);
	}
	fprintf (output, "]]\nb_ub=[%f,", -Econstraint);
	for (i = 0; i < Ncons-1; i ++) fprintf (output, "-0.01,");
	fprintf (output, "-0.01");
	fprintf (output, "]\nA_eq=[");
	for (k = 0; k < cp.N; k ++) {
		j = 0;
		fprintf (output, "[");
		for (i = 0; i < Nphase; i ++)  {
			if (pha[i].mark == 1) {
				mark = 0;
				for (l = 0; l < pha[i].N; l ++) {
					if (strcmp (pha[i].element[l], cp.element[k]) == 0) {
						fprintf (output, "%.3f", pha[i].coe[l]);
							mark = 1;
					}
				}
				if (mark == 0) fprintf (output, "0.000");
					if (j < Ncp-1) fprintf (output, ",");
				j ++;
			}
		}
		fprintf (output, "]");
		if (k < cp.N - 1) fprintf (output, ",\n");
	}
	fprintf (output, "]\nb_eq=[");
	for (i = 0; i < cp.N - 1; i ++)  fprintf (output, "%.3f,", cp.coe[i]);
	fprintf (output, "%.3f", cp.coe[cp.N - 1]);
	fprintf (output, "]\nbounds=[");
	for (i = 0; i < Ncp-1; i ++) fprintf (output, "[0,None],");
	fprintf (output, "[0,None]");
	fprintf (output, "]\nres = linprog(c,A_ub=A_ub,b_ub=b_ub,A_eq=A_eq,b_eq=b_eq,bounds=bounds)\nfile = open(\"result.dat\", \"w\")\nfun = str(res.fun)\nline = '\\n'\nx = str(res.x)\nfile.write(fun)\nfile.write(line)\nfile.write(x)\nfile.close()\n");
	fclose (output);
	system ("cd out && python3 linprog.py");
	sprintf (line, "sed -i \"s/ /\\n/g\" out/result.dat");
	system (line);
	input = fopen("out/result.dat", "r");
	fgets (line, 1024, input);
	sscanf (line, "%lg", &Emin);
	if (Emin > Esim_sub) {
		fclose (input);
		return (Emin);
	}
	AllocMem (comb_coe, Ncp, double);
	i = 0;
	while (1) {
		if (fgets (line, 1024, input) == 0) break;
		if ((line[0] >= 48 && line[0] <= 57) || line[0] == 45) {
			sscanf (line, "%lg", &comb_coe[i]);
			i ++;
		}
	}
	printf ("\nThe combination of raw compounds:   ");
	j = 0;
	for (i = 0; i < Nphase; i ++)  {
		pha[i].mark_imp = 0;
		if (pha[i].mark == 1) {
			if (fabs(comb_coe[j]) > 1.e-10) {
				pha[i].comb_coe = comb_coe[j];
				printf ("%.3f %s         ", comb_coe[j], pha[i].name);
				if (path_flag == 1 && choice == 2) {
					path_file = fopen ("out/2/optimized_synthesis_reactants", "a+");
					fprintf (path_file, "%.3f %s         ", comb_coe[j], pha[i].name);
					fclose (path_file);
				}
				if (i != std_id) pha[i].mark_imp = 1;
			} else pha[i].comb_coe = 0.;
			j ++;
		}
	}
	path_flag ++;
	printf ("\n");
	rewind (input);
	fgets (line, 1024, input);
	sscanf (line, "%lg", &Emin);
	printf ("Emin = %.2f\n", Emin);
	fclose (input);
	free (comb_coe);

	return (Emin);
}

void Start ()
{
printf("                                                                                    \n");
	printf("                                    ^  =  ^                                         \n");
	printf("                                  *         *                                       \n");
	printf("                                 *  _     _  *                                      \n");
	printf("                                *  (@  .  @)  *                                     \n");
	printf("                                *             *                                     \n");
	printf("                                 *   = = =   *                                      \n");
	printf("*------------------------------oOOo---------oOOo-----------------------------------*\n");
    printf("*                                 Thermo-lp                                        *\n");
    printf("*                           Current Version: 1.0                                   *\n");
	printf("*           Developed by: Nan Li (ln906061119@stu.xjtu.edu.cn)                     *\n");
    printf("*                         Xianghui Feng (fengxh01@stu.xjtu.edu.cn)                 *\n");
    printf("*                    Sch of Electrical Engineering, XJTU, Xian, CHINA              *\n");
    printf("*         A program to determine and analyze the synthesizing of MAX phases        *\n");
    printf("*----------------------------------------------------------------------------------*\n"); 
    printf("////////////////////////////////////////////////////////////////////////////////////\n"); 
	printf("*==================================================================================*\n");
    printf("*                        PROPERTIES COMPUTED BY PROGRAM                            *\n");
    printf("*==================================================================================*\n");
    printf("*                       1) obtain Gibbs free energy                                *\n"); 
    printf("*                       2) analyze synthesis prospect                              *\n");
    printf("*                       3) predict impurities                                      *\n");
    printf("*==================================================================================*\n");
}


void ReadINPUT ()
{
	FILE *input;
	char line[1024], *words;
	int i, size;

	input = fopen ("in/INPUT", "r");
	fgets (line, 1024, input);
	sscanf (line, "%d", &choice);
        fgets (line, 1024, input);
	sscanf (line, "%d", &cp.N);
	fgets (line, 1024, input);
	for (i = 0; i < cp.N; i ++) {
		sscanf (line, "%s", cp.element[i]);
		size = strlen (cp.element[i]);
		strcpy (line, line + size + 1);
		puts (cp.element[i]);
	}
	fgets (line, 1024, input);
	words = strtok(line,"' \t\n\r\f");
	for (i = 0; i < cp.N; i ++) {
		cp.coe[i] = atof (words);
		words = strtok(NULL,"' \t\n\r\f");
		printf ("%f\n", cp.coe[i]);
	}
	fgets (line, 1024, input);
	sscanf (line, "%d", &gp_type_id);
	switch (gp_type_id) {
		case 1:
			strcpy (gp_type, "o-MAX");
			break;
		case 2:
			strcpy (gp_type, "SQS");
			break;
		case 3:
			strcpy (gp_type, "i-MAX");
			break;
		case 4:
			strcpy (gp_type, "\0");
			break;
		default:
			break;
	}
	puts (gp_type);
	fgets (line, 1024, input);
	sscanf (line, "%lg", &temperature);
	printf ("%.2f\n", temperature);
	fclose (input);
}
