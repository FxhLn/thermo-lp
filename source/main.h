#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "gibbs.h"

//the allocations of one- and two- and three- dimensional arrays of any kind of variable or structure
#define AllocMem(a, n, t) 				\
	a = (t *) malloc ((n) * sizeof (t))
#define AllocMem2(a, n1, n2, t)				\
	AllocMem (a, n1, t *);				\
	AllocMem (a[0], (n1) * (n2), t);		\
	for (k = 1; k < n1; k ++) a[k] = a[k - 1] + n2;
#define AllocMem3(a, n1, n2, n3, t)					\
	AllocMem2(a, n1, n2, t *);					\
	AllocMem(a[0][0], (n1) * (n2) * (n3), t);			\
	for (k = 1; k < n1; k ++) a[k][0] = a[k - 1][0] + n2 * n3;	\
	for (j = 0; j < n1; j++){					\
		for (k = 1; k < n2; k++) a[j][k] = a[j][k - 1] + n3;	\
	}
#define ReAllocMem(a, n, t) 				\
	a = (t *) realloc (a, (n) * sizeof (t))
#define ReAllocMem2(a, n1, n2, t)				\
	ReAllocMem (a, n1, t *);				\
	ReAllocMem (a[0], (n1) * (n2), t);		\
	for (k = 1; k < n1; k ++) a[k] = a[k - 1] + n2;

typedef struct {
	char name[128];
	int N;
	char element[6][10];
	double coe[6], comb_coe;
	char type[10];
	int mark_cp, mark_exp, mark_exp_easy, mark, mark_imp;
	double energy; //--eV

	int NE;
	double E[2000], T[2000];
} Phase;

void init (int argc, char **argv);
void ReadName ();
void CompetingPhases ();
void Project ();
void SearchImpurityPhase ();
double LinearProgramming (double Econstraint, double **coe_cons, int Ncons);
void Start ();
void ReadINPUT ();

#endif
